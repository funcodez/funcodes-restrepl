// /////////////////////////////////////////////////////////////////////////////
// FUNCODES.CLUB
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.restrepl;

import static org.refcodes.cli.CliSugar.*;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintStream;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.refcodes.archetype.CliHelper;
import org.refcodes.cli.Example;
import org.refcodes.cli.Flag;
import org.refcodes.cli.Option;
import org.refcodes.cli.Term;
import org.refcodes.controlflow.ThreadingModel;
import org.refcodes.data.AsciiColorPalette;
import org.refcodes.exception.Trap;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerFactorySingleton;
import org.refcodes.properties.Properties;
import org.refcodes.properties.ext.application.ApplicationProperties;
import org.refcodes.rest.HttpExceptionHandling;
import org.refcodes.rest.HttpRestServer;
import org.refcodes.rest.RestRequestEvent;
import org.refcodes.rest.RestfulHttpServer;
import org.refcodes.runtime.Correlation;
import org.refcodes.textual.Font;
import org.refcodes.textual.FontFamily;
import org.refcodes.textual.FontStyle;
import org.refcodes.textual.RandomTextGenerartor;
import org.refcodes.textual.RandomTextMode;
import org.refcodes.web.BadRequestException;
import org.refcodes.web.BasicAuthCredentials;
import org.refcodes.web.BasicAuthCredentialsBuilder;
import org.refcodes.web.BasicAuthResponse;
import org.refcodes.web.HttpBodyMap;
import org.refcodes.web.HttpMethod;
import org.refcodes.web.HttpServerResponse;
import org.refcodes.web.HttpStatusException;
import org.refcodes.web.InternalServerErrorException;
import org.refcodes.web.MediaType;
import org.refcodes.web.UnauthorizedException;

import jdk.jshell.JShell;
import jdk.jshell.SnippetEvent;

/**
 * Simple application providing a RESTful interface for the JSHELL REPL ([R]ead,
 * [E]xecute, [P]rint, [L]oop..
 */
public class Main {

	private static final RuntimeLogger LOGGER = RuntimeLoggerFactorySingleton.createRuntimeLogger();

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String NAME = "restrepl";
	private static final String TITLE = "<<< RestREPL >>>";
	private static final String DEFAULT_CONFIG = NAME + ".ini";
	private static final String LICENSE_NOTE = "Licensed under GNU General Public License, v3.0 and Apache License, v2.0";
	private static final String COPYRIGHT = "Copyright (c) by CLUB.FUNCODES (see [https://www.funcodes.club])";
	private static final char[] BANNER_PALETTE = AsciiColorPalette.MAX_LEVEL_GRAY.getPalette();
	private static final Font BANNER_FONT = new Font( FontFamily.DIALOG, FontStyle.BOLD );
	private static final String DESCRIPTION = "RestREPL provides a RESTful interface for JSHELL's [R]ead, [E]xecute, [P]rint, [L]oop interaction. Get inspired by [https://bitbucket.org/funcodez].";

	private static final String ANONYMOUS = "(anonymous)";
	private static final int DEFAULT_PORT = 5161;
	private static final int DEDAULT_ADMIN_PORT = 5162;
	private static final int SHUTDOWN_LATENCY_MILLIS = 300;
	private static final String ATTR_ERR_STREAM = "err";
	private static final String ATTR_OUT_STREAM = "out";
	private static final String ATTR_EVALUATE_EXPRESSION = "eval";
	private static final String ATTR_INPUT_STREAM = "in";
	private static final String PARAM_SESSION_ID = "sessionId";

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private Map<String, ShellSession> _shellSessions = new HashMap<>();
	private RandomTextGenerartor _textGenerator = new RandomTextGenerartor().withRandomTextMode( RandomTextMode.ALPHANUMERIC ).withColumnWidth( 8 );

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	public Main( String... args ) {

		// ---------------------------------------------------------------------
		// CLI:
		// ---------------------------------------------------------------------

		final Option<Integer> thePortArg = intOption( 'p', "port", "port", "Sets the port for the server" );
		final Option<Integer> theAdminPortArg = intOption( 'a', "admin-port", "admin/port", "The admin-port on which to listen for shutdown" );
		final Flag theSysInfoFlag = sysInfoFlag( false );
		final Flag theHelpFlag = helpFlag();
		final Flag theVerboseFlag = verboseFlag();
		// @formatter:off
		final Term theArgsSyntax = any ( xor( 
			optional( thePortArg, theAdminPortArg, theVerboseFlag ),
			any( xor( theHelpFlag, theSysInfoFlag ) ) )
		);
		final Example[] theExamples = examples(
			example( "To show the help text", theHelpFlag ),
			example( "To print the system info", theSysInfoFlag )
		);
		final CliHelper theCliHelper = CliHelper.builder().
			withArgs( args ).
			// withArgs( args, ArgsFilter.D_XX ).
			withArgsSyntax( theArgsSyntax ).
			withExamples( theExamples ).
			withFilePath( DEFAULT_CONFIG ). // Must be the name of the default (template) configuration file below "/src/main/resources"
			withResourceClass( Main.class ).
			withName( NAME ).
			withTitle( TITLE ).
			withDescription( DESCRIPTION ).
			withLicense( LICENSE_NOTE ).
			withCopyright( COPYRIGHT ).
			withBannerFont( BANNER_FONT ).
			withBannerFontPalette( BANNER_PALETTE ).
			withLogger( LOGGER ).build();
		// @formatter:on

		final ApplicationProperties theArgsProperties = theCliHelper.getApplicationProperties();

		// ---------------------------------------------------------------------
		// MAIN:
		// ---------------------------------------------------------------------

		try {
			openAdminServer( theArgsProperties );
			openServer( theArgsProperties );
		}
		catch ( Exception e ) {
			LOGGER.error( Trap.asMessage( e ), e );
			System.exit( e.hashCode() % 0xFF );
		}
	}

	public static void main( String args[] ) {
		new Main( args );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HTTP:
	// /////////////////////////////////////////////////////////////////////////

	private void onCreateSession( RestRequestEvent aRequest, HttpServerResponse aResponse ) throws HttpStatusException {
		try {
			String theSessionId = aRequest.getRequest().get( PARAM_SESSION_ID );
			synchronized ( _shellSessions ) {
				if ( theSessionId != null && _shellSessions.containsKey( theSessionId ) ) {
					LOGGER.info( "Session with Session-TID <" + theSessionId + "> already in use !!!" );
					throw new BadRequestException( "Session with Session-TID <" + theSessionId + "> already in use !!!" );
				}

				if ( theSessionId == null ) {
					theSessionId = _textGenerator.next();
					while ( _shellSessions.containsKey( theSessionId ) ) {
						theSessionId = _textGenerator.next();
					}
					LOGGER.info( "Generating session with Session-TID <" + theSessionId + "> ..." );
				}
				// -------------------------------------------------------------
				final String theAlias = aRequest.getRequest().get( "alias", theSessionId );
				String theIdentity = aRequest.getRequest().get( "identity" );
				if ( theIdentity == null || theIdentity.isEmpty() ) {
					theIdentity = theSessionId;
				}
				String theSecret = aRequest.getRequest().get( "secret" );
				LOGGER.info( "Creating session with Session-TID <" + theSessionId + "> and Session-Alias <" + theAlias + "> ..." );
				// -------------------------------------------------------------
				final PipedOutputStream theUserOutStream = new PipedOutputStream();
				final PipedInputStream theShellInStream = new PipedInputStream( theUserOutStream );
				final PipedInputStream theUserStdInStream = new PipedInputStream();
				final PipedOutputStream theShellStdOutStream = new PipedOutputStream( theUserStdInStream );
				final PipedInputStream theUserErrInStream = new PipedInputStream();
				final PipedOutputStream theShellErrOutStream = new PipedOutputStream( theUserErrInStream );
				// -------------------------------------------------------------
				final JShell theShell = JShell.builder().in( theShellInStream ).out( new PrintStream( theShellStdOutStream ) ).err( new PrintStream( theShellErrOutStream ) ).build();
				theShell.onShutdown( this::onShutdown );
				final ShellSession theShellSession = new ShellSession( theAlias, theIdentity, theSecret, theShell, theUserOutStream, theUserStdInStream, theUserErrInStream );
				_shellSessions.put( theSessionId, theShellSession );

			}
			HttpBodyMap theBody = new HttpBodyMap().withPut( PARAM_SESSION_ID, theSessionId );
			aResponse.setResponse( theBody );
		}
		catch ( IOException e ) {
			throw new InternalServerErrorException( Trap.asMessage( e ) );
		}
	}

	private void onEvaluateSession( RestRequestEvent aRequest, HttpServerResponse aResponse ) throws HttpStatusException {
		try {
			// -----------------------------------------------------------------
			String theSessionId = aRequest.getWildcardReplacement( PARAM_SESSION_ID );
			LOGGER.info( "Evaluating request for Session-TID <" + theSessionId + "> ..." );
			final ShellSession theShellSession;
			synchronized ( _shellSessions ) {
				theShellSession = _shellSessions.get( theSessionId );
			}
			if ( theShellSession == null ) {
				LOGGER.warn( "No such session <" + theSessionId + "> !!!" );
				throw new BadRequestException( "No such session <" + theSessionId + "> !!!" );
			}
			// -----------------------------------------------------------------
			final BasicAuthCredentials theSessionCredentials = new BasicAuthCredentials( theShellSession.identity, theShellSession.secret );
			final BasicAuthCredentials theRequestCredentials = aRequest.getHeaderFields().getBasicAuthCredentials();
			if ( theSessionCredentials.getSecret() != null && theSessionCredentials.getSecret().length() != 0 ) {
				if ( theSessionCredentials != theRequestCredentials ) {
					throw new UnauthorizedException( "Access denied for unauthorized identity <" + theRequestCredentials.getIdentity() + ">!" );
				}
			}
			// -----------------------------------------------------------------
			final HttpBodyMap theBody = new HttpBodyMap();
			final String theIn = aRequest.getRequest().get( ATTR_INPUT_STREAM );
			final String theEval = aRequest.getRequest().get( ATTR_EVALUATE_EXPRESSION );
			// -----------------------------------------------------------------
			LOGGER.info( "Feeding input <" + theIn + ">  ..." );
			if ( theIn != null && theIn.length() != 0 ) {
				theShellSession.userOutStream.write( theIn.getBytes() );
				theShellSession.userOutStream.flush();
			}
			// -----------------------------------------------------------------
			if ( theEval != null && theEval.length() != 0 ) {
				LOGGER.info( "Evaluating expression <" + theEval + ">  ..." );
				final List<SnippetEvent> theEvents = theShellSession.jShell.eval( theEval );
				SnippetEvent eEvent;
				for ( int i = 0; i < theEvents.size(); i++ ) {
					eEvent = theEvents.get( i );
					LOGGER.info( "Result[" + i + "] = " + eEvent.toString() );
					theBody.put( "result/" + i + "/value", eEvent.value() );
					theBody.put( "result/" + i + "/isSignatureChange", eEvent.isSignatureChange() + "" );
					theBody.insertTo( "result/" + i + "/causeSnippet", eEvent.causeSnippet() );
					theBody.insertTo( "result/" + i + "/exception", eEvent.exception() );
					theBody.insertTo( "result/" + i + "/previousStatus", eEvent.previousStatus() );
					theBody.insertTo( "result/" + i + "/snippet", eEvent.snippet() );
					theBody.insertTo( "result/" + i + "/status", eEvent.status() );
				}
			}
			// -----------------------------------------------------------------
			byte[] theBytes;
			String theStdOut = null;
			int available = theShellSession.userStdInStream.available();
			if ( available > 0 ) {
				theBytes = new byte[available];
				theShellSession.userStdInStream.read( theBytes, 0, available );
				theStdOut = new String( theBytes );
			}
			LOGGER.info( "Standard-Out = <" + theStdOut + ">  ..." );
			theBody.put( ATTR_OUT_STREAM, theStdOut );
			// -----------------------------------------------------------------
			String theStdErr = null;
			available = theShellSession.userErrInStream.available();
			if ( available > 0 ) {
				theBytes = new byte[available];
				theShellSession.userErrInStream.read( theBytes, 0, available );
				theStdErr = new String( theBytes );
			}
			LOGGER.info( "Standard-Error = <" + theStdErr + ">  ..." );
			theBody.put( ATTR_ERR_STREAM, theStdErr );
			// -----------------------------------------------------------------
			aResponse.setResponse( theBody );
			// -----------------------------------------------------------------
		}
		catch ( IOException e ) {
			throw new InternalServerErrorException( Trap.asMessage( e ) );
		}
	}

	private void onListSessions( RestRequestEvent aRequest, HttpServerResponse aResponse ) throws HttpStatusException {
		// ---------------------------------------------------------------------
		LOGGER.info( "Listing sessions ..." );
		// ---------------------------------------------------------------------
		final HttpBodyMap theBody = new HttpBodyMap();
		final Map<String, ShellSession> theShellSessions;
		synchronized ( _shellSessions ) {
			theShellSessions = new HashMap<>( _shellSessions );
		}
		int i = 0;
		for ( String eKey : theShellSessions.keySet() ) {
			theBody.insertTo( "sessions/" + i + "/" + eKey, theShellSessions.get( eKey ) );
			i++;
		}
		aResponse.setResponse( theBody );
		// ---------------------------------------------------------------------
	}

	private void onDeleteSession( RestRequestEvent aRequest, HttpServerResponse aResponse ) throws HttpStatusException {
		// ---------------------------------------------------------------------
		final String theSessionId = aRequest.getWildcardReplacement( PARAM_SESSION_ID );
		LOGGER.info( "Deleting session with Session-TID <" + theSessionId + "> ..." );
		final ShellSession theShellSession;
		synchronized ( _shellSessions ) {
			theShellSession = _shellSessions.get( theSessionId );
		}
		if ( theShellSession != null ) {
			theShellSession.close();
			_shellSessions.remove( theSessionId );
		}
		else {
			LOGGER.warn( "No such session <" + theSessionId + "> !!!" );
			throw new BadRequestException( "No such session <" + theSessionId + "> !!!" );
		}
		// ---------------------------------------------------------------------

		// ---------------------------------------------------------------------
		HttpBodyMap theBody = new HttpBodyMap().withPut( "invalidatedId", theSessionId );
		aResponse.setResponse( theBody );
		// ---------------------------------------------------------------------
	}

	private void onShutdown( JShell aShell ) {
		ShellSession eShellSession;
		String theSessionId = null;
		for ( String eSessionId : _shellSessions.keySet() ) {
			eShellSession = _shellSessions.get( eSessionId );
			if ( eShellSession != null && eShellSession.jShell == aShell ) {
				theSessionId = eSessionId;
				break;
			}
		}
		// ---------------------------------------------------------------------
		if ( theSessionId != null ) {
			LOGGER.info( "Shutting down shell for Session-TID <" + theSessionId + "> ..." );
			synchronized ( _shellSessions ) {
				ShellSession theShellSession = _shellSessions.get( theSessionId );
				if ( theShellSession != null ) {
					theShellSession.close();
					_shellSessions.remove( theSessionId );
				}
			}
		}
		else {
			LOGGER.warn( "Shell <" + aShell + "> with unknown Session-TID was closed!" );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	public static class ShellSession {
		final JShell jShell;
		final PipedOutputStream userOutStream;
		final PipedInputStream userStdInStream;
		final PipedInputStream userErrInStream;
		final public String alias;
		final public transient String identity;
		final public transient String secret;

		ShellSession( String aAlias, String aIdentity, String aSecret, JShell aShell, PipedOutputStream aUserOutStream, PipedInputStream aUserStdInStream, PipedInputStream aUserErrInStream ) {
			alias = aAlias;
			identity = aIdentity;
			secret = aSecret;
			jShell = aShell;
			userOutStream = aUserOutStream;
			userStdInStream = aUserStdInStream;
			userErrInStream = aUserErrInStream;
		}

		void close() {
			try {
				jShell.close();
			}
			catch ( Exception ignore ) {}

			try {
				userOutStream.close();
			}
			catch ( Exception ignore ) {}
			try {
				userStdInStream.close();
			}
			catch ( Exception ignore ) {}
			try {
				userErrInStream.close();
			}
			catch ( Exception ignore ) {}

		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// DAEMONS:
	// /////////////////////////////////////////////////////////////////////////

	private void openServer( Properties aProperties ) throws IOException {
		final RestfulHttpServer theServer = new HttpRestServer( ThreadingModel.MULTI ).withHttpExceptionHandling( HttpExceptionHandling.REPLACE );
		// ---------------------------------------------------------------------
		final BasicAuthCredentialsBuilder theCredentials = aProperties.toType( BasicAuthCredentialsBuilder.class );
		if ( theCredentials != null ) {
			if ( theCredentials.getSecret() != null && theCredentials.getSecret().length() != 0 && ( theCredentials.getIdentity() == null || theCredentials.getIdentity().isEmpty() ) ) {
				LOGGER.warn( "A security secret has been configured, but no security identity has been configured. Security is disabled!" );
			}
			else if ( theCredentials.getIdentity() != null && theCredentials.getIdentity().length() != 0 && ( theCredentials.getSecret() == null || theCredentials.getSecret().isEmpty() ) ) {
				LOGGER.warn( "A security identity <" + theCredentials.getIdentity() + "> has been configured, but no security secret has been configured. Security is disdabled!" );
			}
			else if ( theCredentials.getIdentity() != null && theCredentials.getIdentity().length() != 0 && theCredentials.getSecret() != null && theCredentials.getSecret().length() != 0 ) {
				theServer.onBasicAuthRequest( ( InetSocketAddress aLocalAddress, InetSocketAddress aRemoteAddress, HttpMethod aHttpMethod, String aLocator, BasicAuthCredentials aCredentials, String aRealm ) -> {
					if ( "/repl/sessions".equals( aLocator ) ) {
						Correlation.REQUEST.pullId();
						final String theIdentity = aCredentials != null ? aCredentials.getIdentity() : "(unknown)";
						LOGGER.info( "Requesting Basic-Authentication from <" + aRemoteAddress.toString() + "> for \"" + theIdentity + "\" on " + aLocalAddress.toString() + " with realm <" + aRealm + ">." );
						if ( theIdentity == null || theIdentity.isEmpty() ) {
							return BasicAuthResponse.BASIC_AUTH_REQUIRED;
						}
						if ( !aCredentials.equals( theCredentials ) ) {
							return BasicAuthResponse.BASIC_AUTH_FAILURE;
						}
					}
					return BasicAuthResponse.BASIC_AUTH_SUCCESS;
				} );
			}
		}
		// ---------------------------------------------------------------------
		// theServer.withOnHttpException( ( req, res, ex, code ) -> {
		//	res.setResponse( new HttpBodyMap().withPut( "message", ex.getMessage() ) );
		// } );
		int theMaxConnections = -1;
		if ( aProperties.containsKey( "maxConnections" ) ) {
			theMaxConnections = aProperties.getInt( "maxConnections" );
		}
		// ---------------------------------------------------------------------
		theServer.onPost( "/repl/sessions", this::onCreateSession ).open();
		theServer.onGet( "/repl/sessions", this::onListSessions ).open();
		theServer.onPut( "/repl/sessions/${" + PARAM_SESSION_ID + "}", this::onEvaluateSession ).open();
		theServer.onDelete( "/repl/sessions/${" + PARAM_SESSION_ID + "}", this::onDeleteSession ).open();
		// ---------------------------------------------------------------------
		int thePort = DEFAULT_PORT;
		if ( aProperties.containsKey( "port" ) ) {
			thePort = aProperties.getInt( "port" );
		}
		// ---------------------------------------------------------------------
		LOGGER.info( "Starting service on port <" + thePort + "> ..." );
		theServer.open( thePort, theMaxConnections );
	}

	private void openAdminServer( Properties aProperties ) throws IOException {
		final RestfulHttpServer theServer = new HttpRestServer( ThreadingModel.SINGLE );
		// ---------------------------------------------------------------------
		final BasicAuthCredentialsBuilder theCredentials = aProperties.toType( "admin", BasicAuthCredentialsBuilder.class );
		if ( theCredentials != null ) {
			if ( theCredentials.getSecret() != null && theCredentials.getSecret().length() != 0 && ( theCredentials.getIdentity() == null || theCredentials.getIdentity().isEmpty() ) ) {
				LOGGER.warn( "A security secret has been configured, but no security identity has been configured. Security is disabled!" );
			}
			else if ( theCredentials.getIdentity() != null && theCredentials.getIdentity().length() != 0 && ( theCredentials.getSecret() == null || theCredentials.getSecret().isEmpty() ) ) {
				LOGGER.warn( "A security identity <" + theCredentials.getIdentity() + "> has been configured, but no security secret has been configured. Security is disabled!" );
			}
			else if ( theCredentials.getIdentity() != null && theCredentials.getIdentity().length() != 0 && theCredentials.getSecret() != null && theCredentials.getSecret().length() != 0 ) {
				theServer.onBasicAuthRequest( ( InetSocketAddress aLocalAddress, InetSocketAddress aRemoteAddress, HttpMethod aHttpMethod, String aLocator, BasicAuthCredentials aCredentials, String aRealm ) -> {
					Correlation.REQUEST.pullId();
					final String theIdentity = aCredentials != null ? aCredentials.getIdentity() : ANONYMOUS;
					LOGGER.info( "Requesting Basic-Authentication from <" + aRemoteAddress.toString() + "> for \"" + theIdentity + "\" on " + aLocalAddress.toString() + " with realm <" + aRealm + ">." );
					if ( theIdentity == null || theIdentity.isEmpty() ) {
						return BasicAuthResponse.BASIC_AUTH_REQUIRED;
					}
					if ( !aCredentials.equals( theCredentials ) ) {
						return BasicAuthResponse.BASIC_AUTH_FAILURE;
					}
					return BasicAuthResponse.BASIC_AUTH_SUCCESS;
				} );
			}
		}
		// ---------------------------------------------------------------------

		theServer.onGet( "/shutdown", ( aRequest, aResponse ) -> { LOGGER.info( "Shutting down ..." ); aResponse.getHeaderFields().putContentType( MediaType.APPLICATION_JSON ); aResponse.setResponse( "Shutting down ... bye!" ); theServer.closeIn( SHUTDOWN_LATENCY_MILLIS ); Executors.newSingleThreadScheduledExecutor().schedule( () -> System.exit( 0 ), SHUTDOWN_LATENCY_MILLIS, TimeUnit.MICROSECONDS ); } ).open();
		int thePort = DEDAULT_ADMIN_PORT;
		if ( aProperties.containsKey( "admin/port" ) ) {
			thePort = aProperties.getInt( "admin/port" );
		}
		LOGGER.info( "Starting admin server on port <" + thePort + "> ..." );
		theServer.open( thePort );
	}
}
